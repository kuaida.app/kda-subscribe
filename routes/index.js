var express = require('express');
var router = express.Router();
const https = require('https');

const getData = (key) => {
  return new Promise((resolve, reject) => {
    let _data = '';
    if(!key) return reject('缺少参数');
    var kdaUrl = `https://kuaida.app/api/user/account/subscribe/${key}?type=shadowrocket&ip=0&flow=0`;
    const req = https.get( kdaUrl , res => {
      res.setEncoding('utf8');
      res.on('data', (chunk) => {
        _data+=chunk;
      });
      res.on('end', () => {
        resolve(toJSON(_data, 1, '请求成功'));
      });
    });
    req.on('timeout', () => {
      reject(toJSON(mull, 0, '请求超时'))
    });
    req.on('error', (e) => {
      console.dir('https request error:', e);
      reject(toJSON(mull, 0, '请求出错了'))
    });
    req.end();
    })
};


function toJSON(data=null, code = 0, msg=""){
	return {code, data, msg}
}

function b64DecodeUnicode(str) {
    return Buffer.from(str, 'base64').toString('utf8');
}


/* GET home page. */
router.get('/', async function(req, res, next) {
	try{
		const key = req.query.key;
		const result = await getData(key);
		if(result.code !== 1){
			res.render('index', { data: result.msg});
		}else{
			const unicode = b64DecodeUnicode(result.data);
			res.render('index', { data: unicode});
		}
	}catch(err){
		console.log('render err:', err);
		res.render('index', { data: '出错了' });
	}

  
});

module.exports = router;



