var express = require('express');
var router = express.Router();
var https = require('https');
var  os = require("os");

const getData = (url) => {
  return new Promise((resolve, reject) => {
    if(!url) return reject('缺少参数');
    let _data = '';
    url = decodeURIComponent(url);
    const arr = url.match(/(?<=subscribe\/).*?(?=\?type)/);
    if(!arr || !arr.length) return  reject(toJSON(mull, 0, '链接异常'));
    var key = arr[0];

    const req = https.get( url , res => {
      res.setEncoding('utf8');
      res.on('data', (chunk) => {
        _data+=chunk;
      });
      res.on('end', () => {
        resolve(toJSON(_data, 1, '请求成功', {url:'/?key='+key}));
      });
    });
    req.on('timeout', () => {
      reject(toJSON(mull, 0, '请求超时'))
    });
    req.on('error', (e) => {
      console.dir('https request error:', e);
      reject(toJSON(mull, 0, '请求出错了'))
    });
    req.end();
    })
};


function toJSON(data=null, code = 0, msg="", opts = {}){
	return {code, data, msg, ...opts}
}

function b64DecodeUnicode(str) {
    return Buffer.from(str, 'base64').toString('utf8');
}


/* GET home page. */
router.get('/', async function(req, res, next) {
  var fullUrl = req.protocol + '://' + req.get('host');

	try{
		const url = req.query.url;
		const result = await getData(url);

		if(result.code !== 1) return alert(result.msg);

		const unicode = b64DecodeUnicode(result.data);
		res.render('get', { data: unicode, subscribe: fullUrl + result.url});
	}catch(err){
		console.log('render err:', err);
		res.render('get', { data: '出错了' });
	}

  
});

module.exports = router;



